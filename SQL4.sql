/* 
Query all columns for a city in CITY table with the ID 1661.

The CITY table is described as follows:

                CITY
    Field                   Type
    ID                          NUMBER
    NAME                   VARCHAR2(17)
    COUNTRYCODE VARCHAR2(3)
    DISTRICT              VARCHAR2(20)
    POPULATION      NUMBER */

SELECT *
from CITY
WHERE ID = 1661;