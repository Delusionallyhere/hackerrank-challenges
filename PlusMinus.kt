/* HackerRank Plus Minus Challenge from Algorithm Warmup
    Solved by Raymond Ortiz
    
Task :
        Given an array of integers, calculate the ratios of its elements that are positive,
        negative, and zero. Print the decimal value of each fraction on a new line with 6 places
        after the decimal.
        
Example :
        arr = [1, 1, 0, -1, -1]
        There are n = 5 elements, two positive, two negative and one zero. Their ratios are
        2/5 = 0.400000, 2/5 = 0.400000 and 1/5 = 0.200000. Results are printed as:
        0.400000
        0.400000
        0.200000
        
Function Description :
        Complete the plusMinus function in the editor below.
        plusMinus has the following parameter(s):
            int arr[n]: an array of integers
            
Print :
        Print the ratios of positive, negative and zero values in the array. Each value should be 
        printed on a separate line with 6 digits after teh decimal. The function should not return a value.
        
Input Format :
        The first line contains an integer, n, the size of the array.
        The second line contains n space-seperated integers that describe arr[n].
        
Constraints :
        0 < n <= 100
        -100 <= arr[i] <= 100
        
Output Format :
        Print the following 3 lines, each to 6 decimals:
        1. proportion of positive values
        2. proportion of negative values
        3. proportion of zeros
        
Sample Input :
STDIN                           Function
--------                           ----------- 
6                                       arr[] size n = 6
-4 3 -9 0 4 1                   arr = [-4, 3, -9, 0, 4, 1]

Sample Output :
0.500000
0.333333
0.166667

Explanation :
There are 3 positive numbers, 2 negative numbers, and 1 zero in the array.
The proportions of occurrence are positive: 3/6 = 0.500000, negative:
2/6 = 0.333333 and zeros: 1/6 = 0.166667. */


import java.io.*
import java.math.*
import java.security.*
import java.text.*
import java.util.*
import java.util.concurrent.*
import java.util.function.*
import java.util.regex.*
import java.util.stream.*
import kotlin.collections.*
import kotlin.comparisons.*
import kotlin.io.*
import kotlin.jvm.*
import kotlin.jvm.functions.*
import kotlin.jvm.internal.*
import kotlin.ranges.*
import kotlin.sequences.*
import kotlin.text.*
import kotlin.math.*

/*
 * Complete the 'plusMinus' function below.
 *
 * The function accepts INTEGER_ARRAY arr as parameter.
 */

fun plusMinus(arr: Array<Int>): Unit {
    var positiveRatio = 0.00000f
    var negativeRatio = 0.00000f
    var zeroRatio = 0.00000f

    for (i in arr.indices) {
        if (arr[i] > 0.00000f) {
            positiveRatio += 1.00000f
        } else if (arr[i] < 0.00000f){
            negativeRatio += 1.00000f
        } else if (arr[i] == 0.00000f) {
            zeroRatio += 1.00000f
        }
    }

    positiveRatio = positiveRatio.div(arr.size)
    negativeRatio = negativeRatio.div(arr.size) 
    zeroRatio = zeroRatio.div(arr.size)
    
    print("$positiveRatio\n")
    print("$negativeRatio\n")
    print("$zeroRatio\n")
}

fun main(args: Array<String>) {
    val n = readLine()!!.trim().toInt()

    val arr = readLine()!!.trimEnd().split(" ").map{ it.toInt() }.toTypedArray()

    plusMinus(arr)
}
