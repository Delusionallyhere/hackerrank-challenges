/* HackerRank Staircase Challenge from Algorithm Warmup
    Solved by Raymond Ortiz
    
Task :
        Staircase detail
        This is a staircase of size n = 4:
                #
             ##
          ###
        ####

        Its base and height are both equal to n. It is drawan using # symbols and spaces. 
        The last line is not preceded by any spaces.

        Write a program that prints a staircase of size n.
        
Function Description :
        Complete the staircase function in the editor below.
        staircase has the following parameter(s):
            int n: an integer
            
Print :
        Print a staircase as described above.
        
Input Format :
        A single integer, n, denothing the size of the staircase.

Constraints :
        0 <= n <= 100.

Output Format :
        Print a staircase of size n using # symbols and spaces.
        Note:  The last line must have 0 spaces in it.
        
Sample Input :
        6

Sample Output :
                #
             ##
          ###
       ####
    #####
 ######

Explanation :
        The staircase is right-aligned, composed of # symbols and spaces,
        and has a height and width of n = 6. */

import java.io.*
import java.math.*
import java.security.*
import java.text.*
import java.util.*
import java.util.concurrent.*
import java.util.function.*
import java.util.regex.*
import java.util.stream.*
import kotlin.collections.*
import kotlin.comparisons.*
import kotlin.io.*
import kotlin.jvm.*
import kotlin.jvm.functions.*
import kotlin.jvm.internal.*
import kotlin.ranges.*
import kotlin.sequences.*
import kotlin.text.*

/*
 * Complete the 'staircase' function below.
 *
 * The function accepts INTEGER n as parameter.
 */

fun staircase(n: Int): Unit {
    for (i in 1..n) {
        print(" ".repeat((n - i)) + "#".repeat(i) + "\n")
    }

}

fun main(args: Array<String>) {
    val n = readLine()!!.trim().toInt()

    staircase(n)
}
