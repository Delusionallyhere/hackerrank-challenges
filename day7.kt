/* HackerRank Day 7 challenge from 30 Days of Code
    Solved by Raymond Ortiz

    Task:
    Given an array, A, of N integers, print A's elements in reverse order as a single line of space separated numbers.

    Example:
    A = [1, 2, 3, 4]
    Print 4 3 2 1. Each integer is separated by one space.

    Input Format:
    The first line contains an integer, N (the size of our array).
    The second line contains N space-separated integers that describe array A's elements.

    Constraints:
    1 <= N <= 1000
    1 <= A[i] <= 10000, where A[i] is the ith integer in the array.

    Output Format:
    Print the elements of array A in reverse order as a single-line of space-separated numbers.

    Sample Input:
    4
    1 4 3 2

    Sample Output
    2 3 4 1
 */

 import java.io.*
import java.math.*
import java.security.*
import java.text.*
import java.util.*
import java.util.concurrent.*
import java.util.function.*
import java.util.regex.*
import java.util.stream.*
import kotlin.collections.*
import kotlin.comparisons.*
import kotlin.io.*
import kotlin.jvm.*
import kotlin.jvm.functions.*
import kotlin.jvm.internal.*
import kotlin.ranges.*
import kotlin.sequences.*
import kotlin.text.*



fun main(args: Array<String>) {
    val n = readLine()!!.trim().toInt()

    val arr = readLine()!!.trimEnd().split(" ").map{ it.toInt() }.toTypedArray()

    println(arr.reversed().joinToString(" "))
}