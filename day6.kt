/* HackerRank Day 6 challenge from 30 Days of Code
    Solved by Raymond Ortiz

  Task :
    Given a string, S, of length N that is indexed from 0 to N - 1, print its even-indexed and
    odd-indexed characters as 2 space-separated strings on a single line.
    Note: 0 is considered to be an even index.

    Example
    s = adbecf
    print abcdef

    Input Format
    The first line contains an integer, T (the number of test cases).
    Each line i of the T subsequent lines contain a string, S.

    Constraints
    1 <= T <= 10
    2 <= length of S <= 10000

    Output Format
    For each String Sj (where 0 <= j <= T - 1), print Sj's even-indexed characters, followed
    by a space, followed by Sj's odd-indexed characters.

    Sample Input
    2
    Hacker
    Rank

    Sample Output
    Hceakr
    Rnak

    Explanation
    Test Case 0:
    S = "Hacker"
    S[0] = "H"
    S[1] = "a"
    S[2] = "c"
    S[3] = "k"
    S[4] = "e"
    S[5] = "r"

    The even indices are 0, 2 and 4, and the odd indices are 1, 3, and 5. We then print a single line of 2 space-separated 
    strings; the first string contains the ordered characters from S's even indices (Hce), and the second string contains
    the ordered characters from S's odd indices (akr).

    Test Case 1:
    S = "Rank"
    S[0] = "R"
    S[0] = "a"
    S[0] = "n"
    S[0] = "k"

    The even indices are 0 and 2, and the odd indices are 1 and 3. We then print a single line of 2 space-separated
    strings; the first string contains the ordered characters from S's even indices (Rn), and the second string contains
    the ordered characters from S's odd indices (ak).
    
 */

import java.io.*
import java.util.*

fun main(args: Array<String>) {
        var scanner = readLine() ?: return
        var t  = scanner.toInt()
        for (i in 0..t) {
            scanner = readLine() ?: return
            separateIndices(scanner)
        }
}

// function to separate input by indices (odd/even)
fun separateIndices(s: String) {
    for (i in 0 until s.length step 2 ) {
        print(s.get(i))
    }
    print(" ")
    for (i in 1 until s.length step 2) {
        print(s.get(i))
    }
    println()
}